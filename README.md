# Un interpréteur multitexte (brainfuck text files)


l'interpréteur est écrit en ![gnu-forth](gnu-forth.org) et va lire du brainfuck dans une liste de fichiers textes.

# utilisation locale d' " hello world! "

```
./run.sh 


>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++.
Hello World!


```
--------------

# On peut utiliser des arguments mais il y a le hello world par défaut 

- text1.txt
- text2.txt
- text3.txt
- text4.txt

```
0-2412-433-~/GITLAB/dev/dev_gforth_brainfuck $ ./run.sh 


>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++.
Hello World!


0-2413-434-~/GITLAB/dev/dev_gforth_brainfuck $ gforth-fast hello_world.fs txt/text2.txt 


++++++++[>+>++>+++>++++>+++++>++++++>+++++++>++++++++>+++++++++>++++++++++>+++++++++++>++++++++++++>+++++++++++++>++++++++++++++>+++++++++++++++>++++++++++++++++<<<<<<<<<<<<<<<<-]>>>>>>>>>>----.++++<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>+++.---<<<<<<<<<<<<>>>>>>>>>>>>>>-.+<<<<<<<<<<<<<<>>>>>>>>>>>>+++.---<<<<<<<<<<<<>>>>>>>>>>>>>.<<<<<<<<<<<<<>>>>>>>>>>>>>>-.+<<<<<<<<<<<<<<>>>>>>>>>>>>>>--.++<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<>>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<>>>>>>>>>>>>+.-<<<<<<<<<<<<>>>>>>>>>>>>>>--.++<<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<<>>>>>>>>>>>>+.-<<<<<<<<<<<<>>>>>>>>>>>>>+.-<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<>>>>.<<<<>++.--<>>>>>>>>>>>>>++.--<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>>++.--<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>>>>>>>>>>>.<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>++.--<>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>+++.---<<<<<<<<<<<<>>>>>>>>>>>>>>-.+<<<<<<<<<<<<<<>>>>>>>>>>>>+++.---<<<<<<<<<<<<>>>>>>>>>>>>>.<<<<<<<<<<<<<>>>>>>>>>>>>>>-.+<<<<<<<<<<<<<<>>>>>>>>>>>>>>--.++<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<>>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<>>>>>>>>>>>>+.-<<<<<<<<<<<<>>>>>>>>>>>>>>--.++<<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>>----.++++<<<<<<<<<<<<<<>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<>>>>.<<<<>>>>>>>>>>>>>>---.+++<<<<<<<<<<<<<<>>>>>>>>>>>>+.-<<<<<<<<<<<<>>>>>>>>>>>>>+.-<<<<<<<<<<<<<>>>>>>>>>>>>>>+++.---<<<<<<<<<<<<<<.
Le cochon est dans le mais 
je repete 
le cochon est dans le mais

0-2414-435-~/GITLAB/dev/dev_gforth_brainfuck $ gforth-fast hello_world.fs 


>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++.
Hello World!


0-2415-436-~/GITLAB/dev/dev_gforth_brainfuck $
```

# Si on veut utiliser en container

On peut appeler chacun par la commande suivante :

```
0-2296-316-~/GITLAB/dev/dev_gforth_brainfuck $ docker build . -t francoispussault/brainfuck
Sending build context to Docker daemon  371.2kB
Step 1/13 : FROM debian:latest
 ---> 278727474c29
Step 2/13 : ARG EXTRACT="text2.txt"
 ---> Using cache
 ---> 013aa51ec723
Step 3/13 : RUN apt-get -y update && apt-get install -y gforth bash
 ---> Using cache
 ---> 88c7574f7545
Step 4/13 : RUN mkdir -p /opt/BF
 ---> Using cache
 ---> 065690a5b598
Step 5/13 : ADD txt /opt/BF/txt
 ---> Using cache
 ---> b94545555328
Step 6/13 : ADD rpmbuild /opt/BF/rpmbuild
 ---> Using cache
 ---> f929dd147404
Step 7/13 : ADD hello_world.fs /opt/BF/hello_world.fs
 ---> Using cache
 ---> 4975486ddcb2
Step 8/13 : ADD bfi /opt/BF/bfi
 ---> Using cache
 ---> 23de9da7a5ba
Step 9/13 : RUN chmod 750 /opt/BF/bfi
 ---> Using cache
 ---> 4e8eb668eeca
Step 10/13 : RUN cp /usr/bin/gforth* /usr/local/bin/
 ---> Using cache
 ---> 66002179dd76
Step 11/13 : RUN exec  /usr/local/bin/gforth-fast /opt/BF/hello_world.fs 2> /dev/null
 ---> Using cache
 ---> a500e8004cfc
Step 12/13 : RUN exec  /opt/BF/bfi /opt/BF/txt/text4.txt 2> /dev/null
 ---> Using cache
 ---> 2b9b6626b0ff
Step 13/13 : ENTRYPOINT exec /opt/BF/bfi /opt/BF/txt/$EXTRACT 2> /dev/null
 ---> Using cache
 ---> 35f36bd94ed4
Successfully built 35f36bd94ed4
Successfully tagged francoispussault/brainfuck:latest
0-2297-317-~/GITLAB/dev/dev_gforth_brainfuck $ docker run -e EXTRACT=text2.txt francoispussault/gforthbrainfuckinterpreter:latest





Le cochon est dans le mais 
je repete 
le cochon est dans le mais




0-2298-318-~/GITLAB/dev/dev_gforth_brainfuck $ 
```

# nota pour jouer on peut aussi utiliser hsbrainfuck

On peut aussi jouer avec hsbrainfuck depuis sa distribution pour expérimenter comme ici avec `+[<+.>]`

ou tout autre séquences valides en brainfuck.

```
0-2294-313-~/GITLAB/dev/dev_gforth_brainfuck $ echo  "+[<+.>]" > infinite.bf
0-2294-314-~/GITLAB/dev/dev_gforth_brainfuck $ hsbrainfuck < infinite.bf 
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	�123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
^C
0-2294-315-~/GITLAB/dev/dev_gforth_brainfuck $
```

une bonne boucle infinie.
