#! /usr/bin/env bash

runner=$(which gforth-fast)

if [[ ${runner} != *"gforth-fast"* ]] ; then
	 echo "Gforth interpreter not found"
	 exit 1
fi
trap '' SIGINT
$runner  ./hello_world.fs	$@
trap SIGINT
tput cnorm
exit $?
