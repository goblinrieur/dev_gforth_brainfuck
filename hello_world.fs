\ vars
0 value str 0 value size
variable pos variable cur 2variable hellotext
\ words
: array ( n -- [n]0 )
	create 0 do
		 0 ,
	loop
	does> swap cells + ;
1024 array mem	\ current text array length
\ helpers ( to simplify the brain-fuck interpreter & main )
: COLORIZE ( n -- colored terminal )
	27 EMIT ." ["	\ ESC[ 27 is escape character
	0 <# #S #> type \ append number 
	." m" ;		\ append "m" so it becomes \e[31m for n = 31 
: init ( -- ) \ set defaults
	to size to str 0 pos ! 0 cur ! ;
: continue? ( -- )
	 pos @ size < ;
: incr           1 swap +! ;
: decr          -1 swap +! ;
: goto          1- pos ! ;		\ update pos
: cur-char      str pos @ + c@ ;	\ current char
: mem-null?     cur @ mem @ 0= ;	\ null detect
: end-of-loop?  cur-char [char] ] = ;	\ go to the corresponding ]
: localuntil    postpone 0= postpone while ; immediate
\ helper to read chars between brackets but not them
: skip[         [char] [ = if 1+ then ;	\ immediately get next
: skip]         [char] ] = if 1- then ;	\ immediately get previous
: skip-char     cur-char skip[ cur-char skip] ;	\ if skip before|after brackets
: skip-loop	( bool -- )
	true begin
		dup 0= end-of-loop? and localuntil
	skip-char pos incr repeat
	drop ; \ switch like macro
\ define ===>
: exit-if-not postpone if postpone else postpone exit postpone then ; immediate
: ===> postpone over postpone = postpone exit-if-not ; immediate \ evaluate current bf instruction from its char
\ brain-fuck interpreter words
: eval+         [char] +  ===>  cur @ mem incr ;	\ ++
: eval-         [char] -  ===>  cur @ mem decr ;	\ --
: eval.         [char] .  ===>  cur @ mem c@ emit ;	\ outputs current byte
: eval,         [char] ,  ===>  key cur @ mem c! ;	\ input value
: eval>         [char] >  ===>  cur incr ;		\ ptr++
: eval<         [char] <  ===>  cur decr ;		\ ptr--
: eval[         [char] [  ===>  mem-null? if skip-loop else pos @ swap then ;	\ if 0 jump after "]"
: eval]         [char] ]  ===>  swap mem-null? if drop else goto then ;		\ go back to "[" if not zero
\ unitary brain-fuck character interpreter
: eval-char     eval+ eval- eval. eval, eval> eval< eval[ eval] ( and finally ) drop ;
\ full brain-fuck interpreter (loops over unitary one)
: bf-translate ( -- )
	init begin 
		continue? while \ breacking loop condition test on end of string ( last char )
		cur-char eval-char pos incr \ interpret current char and position next one
	repeat ;
\ main
: main ( -- ) \ executes the example brain-fuck hello-world
	argc @ 2 < if \ no file given on command line as additionnal argument
		s" >+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++."
		hellotext 2!	\ register that in hellotext
		cr cr 32 colorize hellotext 2@		\ colorize & loads hellotext
		over over type cr			\ copy hello text & displays it "as is" (green)
		31 colorize bf-translate		\ translate & displays it (red)
	else \ with file name given on argument
		cr cr 32 colorize 1 arg slurp-file type	\ loads file & display it
		31 colorize 1 arg slurp-file bf-translate	\ loads file & display it
	then cr cr false dup colorize (bye)		\ restore colors & exit
;
main
\ https://fr.wikipedia.org/wiki/Brainfuck for history & instructions set
