FROM debian:latest
ARG EXTRACT="text2.txt"
RUN apt-get -y update && apt-get install -y gforth bash 
RUN mkdir -p /opt/BF
ADD txt /opt/BF/txt
ADD hello_world.fs /opt/BF/hello_world.fs
ADD bfi /opt/BF/bfi
RUN chmod 750 /opt/BF/bfi
RUN cp /usr/bin/gforth* /usr/local/bin/ 
RUN exec  /usr/local/bin/gforth-fast /opt/BF/hello_world.fs 2> /dev/null
RUN exec  /opt/BF/bfi /opt/BF/txt/text4.txt 2> /dev/null
ENTRYPOINT exec /opt/BF/bfi /opt/BF/txt/$EXTRACT 2> /dev/null
